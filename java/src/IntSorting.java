public IntSorting {

    private static int recursiveBinarySearch(int[] a, int fromIndex, int toIndex, int key) {
        int high = toIndex - 1;
        if (fromIndex > high)  return -(fromIndex + 1);
        int mid = (fromIndex + high) / 2;
        int midVal = a[mid];
        if (midVal < key)  {
            return recursiveBinarySearch(a, mid + 1, high + 1, key);
        }
        else if (midVal > key) {
            return recursiveBinarySearch(a, fromIndex, mid, key);
        }
        else  {
            return mid;
        }
    }

    public static void binaryInsertionSort(int[] a) {
        for (int i = 1; i < a.length; i++) {
            int currentNum = a[i];
            int index = Math.abs(recursiveBinarySearch(a, 0, i, currentNum) + 1);
            System.arraycopy(a, index, a, index + 1, i - index);
            a[index] = currentNum;
        }
    }
}